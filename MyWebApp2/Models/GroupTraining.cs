﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MyWebApp2.Models.Enums;

namespace MyWebApp2.Models
{
    public class GroupTraining
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string TrainerID { get; set; }
        public TrainingType TrainingType { get; set; }
        public string FitnessCenterID { get; set; }
        public int TrainingDuration { get; set; } //mm 
        public DateTime TrainingDateTime { get; set; }  // dd/MM/yyyy HH:mm
        public int MaxNumberOfVisitors { get; set; }
        public List<string> VisitorsID { get; set; }
        public bool IsDeleted { get; set; }

        public GroupTraining() { }
        public GroupTraining(string iD, string name, string trainerID, TrainingType trainingType, string fitnessCenterID, int trainingDuration, DateTime trainingDateTime, int maxNumberOfVisitors, List<string> visitorsID, bool isDeleted)
        {
            ID = iD;
            Name = name;
            TrainerID = trainerID;
            TrainingType = trainingType;
            FitnessCenterID = fitnessCenterID;
            TrainingDuration = trainingDuration;
            TrainingDateTime = trainingDateTime; //.ToString(); //("dd/MM/yyyy HH:mm");
            MaxNumberOfVisitors = maxNumberOfVisitors;
            VisitorsID = visitorsID;
            IsDeleted = isDeleted;
        }
    }
}