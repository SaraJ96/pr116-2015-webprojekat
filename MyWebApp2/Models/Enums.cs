﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebApp2.Models
{
    public class Enums
    {
        public enum Role
        {
            Vlasnik = 1,
            Trener = 2,
            Posetilac = 3
        }

        public enum Gender
        {
            Zenski = 1,
            Muski = 2
        }

        public enum TrainingType
        {
            Yoga = 1,
            Les_mills_tone = 2,
            Body_pump = 3
        }
    }
}