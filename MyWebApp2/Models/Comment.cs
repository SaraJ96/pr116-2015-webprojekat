﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebApp2.Models
{
    public class Comment
    {
       
            public string ID { get; set; }
        //  public string UsernameOfVisitor { get; set; }
            public User User { get; set; }
            public string FitnessCenterID { get; set; } //
            public string TextOfComment { get; set; }
            public int Grade { get; set; }
            public bool IsCommentApproved { get; set; }

            public Comment() { }

            public Comment(string iD, User user, string fitnessCenterID, string textOfComment, int grade, bool isCommentApproved)
            {
                ID = iD;
                User = user;
                FitnessCenterID = fitnessCenterID;
                TextOfComment = textOfComment;
                Grade = grade;
                IsCommentApproved = isCommentApproved;
            }
        }
    
}