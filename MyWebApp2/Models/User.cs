﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MyWebApp2.Models.Enums;

namespace MyWebApp2.Models
{
    public class User
    {
        public string ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        //   public string DateOfBirth { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Role Role { get; set; }

        public List<string> VisitorRegisteredGroupTrainingsID { get; set; }
        public Dictionary<string, GroupTraining> CoachGroupTrainingsID { get; set; }

        public string CoachFitnessCenterID { get; set; }
        public List<string> OwnerFitnessCenterID { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsLogg { get; set; }

        public User() { }

        public User(string id, string username, string password, string name, string lastname, Gender gender, string email, DateTime dateOfBirth, Role role, List<string> visitorRegisteredGroupTrainingsID, Dictionary<string, GroupTraining> coachGroupTrainingsID, string coachFitnessCenterID, List<string> ownerFitnessCenterID, bool isDeleted)
        {
            ID = id;
            Username = username;
            Password = password;
            Name = name;
            Lastname = lastname;
            Gender = gender;
            Email = email;
            DateOfBirth = dateOfBirth; //.ToString("dd/MM/yyyy");
            Role = role;

            VisitorRegisteredGroupTrainingsID = visitorRegisteredGroupTrainingsID;
            CoachGroupTrainingsID = coachGroupTrainingsID;

            CoachFitnessCenterID = coachFitnessCenterID;
            OwnerFitnessCenterID = ownerFitnessCenterID;

            IsDeleted = isDeleted;
            IsLogg = false;
        }
    }

    public class Log
    {
        public string ID { get; set; }
        public string GroupTrainingID { get; set; }
        public string VisitorID { get; set; }
        //public bool IsDeleted { get; set; }
        //public bool IsLogg { get; set; }

        public Log() { }
        public Log(string id, string groupTrainingID, string visitorID)
        {
            ID = id;
            GroupTrainingID = groupTrainingID;
            VisitorID = visitorID;
            //IsDeleted = false;
            //IsLogg = true;

        }



        }
    }