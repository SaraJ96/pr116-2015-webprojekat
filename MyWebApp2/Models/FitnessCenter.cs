﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebApp2.Models
{
    public class FitnessCenter
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int YearOfOpening { get; set; }
        public string OwnerUserID { get; set; } //
        public int MonthlyPrice { get; set; }
        public int YearlyPrice { get; set; }
        public int TrainingPrice { get; set; }
        public int GroupPrice { get; set; }
        public int PersonalTrainingPrice { get; set; }
        public bool IsDeleted { get; set; }

        public FitnessCenter() { }

        public FitnessCenter(string iD, string name, string address, int yearOfOpening, string ownerUserID, int monthlyPrice, int yearlyPrice, int trainingPrice, int groupPrice, int personalTrainingPrice, bool isDeleted)
        {
            ID = iD;
            Name = name;
            Address = address;
            YearOfOpening = yearOfOpening;
            OwnerUserID = ownerUserID;
            MonthlyPrice = monthlyPrice;
            YearlyPrice = yearlyPrice;
            TrainingPrice = trainingPrice;
            GroupPrice = groupPrice;
            PersonalTrainingPrice = personalTrainingPrice;
            IsDeleted = isDeleted;
        }
    }
}