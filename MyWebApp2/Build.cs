﻿using MyWebApp2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace MyWebApp2
{
    public class Build
    {
        
        public static Dictionary<string, User> LoadUser()
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/user.json"));
            return JsonConvert.DeserializeObject<Dictionary<string, User>>(jwt) ?? new Dictionary<string, User>();
        }

        public static void AddUsers(User u)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/user.json"));
            var users = JsonConvert.DeserializeObject<Dictionary<string, User>>(jwt) ?? new Dictionary<string, User>();

            users.Add(u.ID, u);
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/user.json"), JsonConvert.SerializeObject(users, Formatting.Indented));
        }

        public static void UpdateUser(User u)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/user.json"));

            var users = JsonConvert.DeserializeObject<Dictionary<string, User>>(jwt) ?? new Dictionary<string, User>();
            foreach (string ID in users.Keys) { if (u.ID.Equals(ID)) { users[ID] = u; break; } }
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/user.json"), JsonConvert.SerializeObject(users, Formatting.Indented));
        }

        public static Dictionary<string, Log> LoadLog()
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/register.json"));
            return JsonConvert.DeserializeObject<Dictionary<string, Log>>(jwt) ?? new Dictionary<string, Log>();
        }

        public static void AddLog(Log l)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/register.json"));
            var reg = JsonConvert.DeserializeObject<Dictionary<string, Log>>(jwt) ?? new Dictionary<string, Log>();

            reg.Add(l.ID, l);
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/register.json"), JsonConvert.SerializeObject(reg, Formatting.Indented));
        }

        public static Dictionary<string, FitnessCenter> LoadFC()
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/fitnessCenter.json"));
            return JsonConvert.DeserializeObject<Dictionary<string, FitnessCenter>>(jwt) ?? new Dictionary<string, FitnessCenter>();
        }

        public static void AddFC(FitnessCenter f)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/fitnessCenter.json"));
            var fitnessCenters = JsonConvert.DeserializeObject<Dictionary<string, FitnessCenter>>(jwt) ?? new Dictionary<string, FitnessCenter>();

            fitnessCenters.Add(f.ID, f);
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/fitnessCenter.json"), JsonConvert.SerializeObject(fitnessCenters, Formatting.Indented));
        }

        public static void UpdateFitnessCenter(FitnessCenter f)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/fitnessCenter.json"));

            var fitnessCenters = JsonConvert.DeserializeObject<Dictionary<string, FitnessCenter>>(jwt) ?? new Dictionary<string, FitnessCenter>();
            foreach (string ID in fitnessCenters.Keys) { if (f.ID.Equals(ID)) { fitnessCenters[ID] = f; break; } }

            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/fitnessCenter.json"), JsonConvert.SerializeObject(fitnessCenters, Formatting.Indented));
        }

        public static Dictionary<string, GroupTraining> LoadGT()
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"));
            return JsonConvert.DeserializeObject<Dictionary<string, GroupTraining>>(jwt) ?? new Dictionary<string, GroupTraining>();
        }

        public static void AddGT(GroupTraining g)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"));
            var groupTrainings = JsonConvert.DeserializeObject<Dictionary<string, GroupTraining>>(jwt) ?? new Dictionary<string, GroupTraining>();

            groupTrainings.Add(g.ID, g);
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"), JsonConvert.SerializeObject(groupTrainings, Formatting.Indented));
        }

        public static void UpdateGroupTraining(GroupTraining g)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"));

            var groupTrainings = JsonConvert.DeserializeObject<Dictionary<string, GroupTraining>>(jwt) ?? new Dictionary<string, GroupTraining>();

            groupTrainings[g.ID] = g;

            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"), JsonConvert.SerializeObject(groupTrainings, Formatting.Indented));
        }

        public static Dictionary<string, Comment> LoadComm()
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/comment.json"));
            return JsonConvert.DeserializeObject<Dictionary<string, Comment>>(jwt) ?? new Dictionary<string, Comment>();
        }

        public static void AddComment(Comment c)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/comment.json"));
            var comments = JsonConvert.DeserializeObject<Dictionary<string, Comment>>(jwt) ?? new Dictionary<string, Comment>();

            comments.Add(c.ID, c);
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/comment.json"), JsonConvert.SerializeObject(comments, Formatting.Indented));
        }

        public static void UpdateComment(Comment c)
        {
            var jsw = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/comment.json"));

            var comments = JsonConvert.DeserializeObject<Dictionary<string, Comment>>(jsw) ?? new Dictionary<string, Comment>();
            foreach (string ID in comments.Keys) { if (c.ID.Equals(ID)) { comments[ID] = c; break; } }

            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/comment.json"), JsonConvert.SerializeObject(comments, Formatting.Indented));
        }

        public static void UserTraining(User u, string trainingID)
        {
            var jwt = File.ReadAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"));

            var groupTrainings = JsonConvert.DeserializeObject<Dictionary<string, GroupTraining>>(jwt) ?? new Dictionary<string, GroupTraining>();
            foreach (GroupTraining g in groupTrainings.Values) { if (trainingID == g.ID) { g.VisitorsID.Add(u.ID); break; } }
            File.WriteAllText(HostingEnvironment.MapPath("~/DataBase/groupTraining.json"), JsonConvert.SerializeObject(groupTrainings, Formatting.Indented));
        }









    }
}