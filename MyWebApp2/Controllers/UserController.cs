﻿using MyWebApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyWebApp2.Controllers
{
    public class UserController : Controller
    {
        // private static List<User> korisnici = new List<User>();

        // GET: User
        public ActionResult GoToRegPg()
        {
            return View("Register");
        }

        public ActionResult Register()
        {

            bool postoji = false;
            Dictionary<string, User> users = (Dictionary<string, User>)HttpContext.Application["users"];

            MyWebApp2.Models.User u = (MyWebApp2.Models.User)Session["user"];

    

            if(users.Values.Any(x => x.Username == Request["username"])) { postoji = true; }

            if(postoji == true) {
                return View("../Home/ErrorPg");
            }

            if(postoji != true) {

                try
                {
                    User us = new User(DateTimeOffset.Now.ToUnixTimeSeconds().ToString(), Request["username"], Request["password"], Request["name"], Request["lastname"], Enums.Gender.Muski, Request["email"], DateTime.Parse(Request["dateOfBirth"]), Enums.Role.Posetilac, new List<string>(), new Dictionary<string, GroupTraining>(), "", new List<string>(), false);

                    users.Add(us.ID, us);

                    HttpContext.Application["users"] = users;

                    Build.AddUsers(us);

                }
                catch(Exception e)
                {
                    Session["message"] = e.Message;

                    return View("../Home/ErrorPg");
                }
            }

            return RedirectToAction("../User/GoToLogInPg");

        }

        public ActionResult GoToLogInPg()
        {
            return View("LogIn");
        }

        public ActionResult LogIn()
        {
            Dictionary<string, User> users = Build.LoadUser();

            var u = new User();
            var us = new User();

            u.IsLogg = false;

            HttpContext.Session["user"] = u;

            string password=Request["password"] != null ? Request["password"] :"";

            string username=Request["username"] !=null ? Request["username"] :"";


            bool postoji = false;

            if(password.Equals(string.Empty) || username.Equals(string.Empty)) { return View("LogIn");}
                foreach(var use in users.Values)  {
                    if( username.Equals(use.Username ) || password.Equals(use.Password) )
                    { us = use; postoji = true;
                    }
                }

                        if(postoji == false) {
                            u.IsLogg = false;

                            Session["user"] = null;

                            return View("../Home/ErrorPg");
                        }
                        else{
        //  if(postoji==true){
                if(password.Equals(us.Password)){
                            u.IsLogg = true;

                            Session["user"] = us;

                            return RedirectToAction("../FitnessCenter/GetFC");
                }
                else {
                    u.IsLogg = false;
                    Session["user"] = null; return View("../Home/ErrorPg"); }

              //  return View("LogIn");
            }
        }
        public ActionResult LogOut()
        {
            Session["user"] = null;
            return View("LogIn");
        }

        public ActionResult GoToUpdateProfilePg()
        {
            return View("UpdateProfile");
        }

        public ActionResult UpdateProfile()
        {

            Dictionary<string, User> users = Build.LoadUser();

            MyWebApp2.Models.User user = (MyWebApp2.Models.User)Session["user"];
            
            //foreach (User u in users.Values)
            //{
            //    if(Request["username"].Equals(u.Username) && !Request["username"].Equals(user.Username))
            //    {   
            //        return View("UpdateProfile");
            //    }
            //    if(Request["email"].Equals(u.Email) && !Request["email"].Equals(user.Email))
            //    {
            //        return View("UpdateProfile");
            //    }
            //}
            foreach(User u in users.Values){
                    if(u.Username==user.Username) {
                        user.Username = Request["username"];
                        user.Password = Request["password"];
                        user.Name = Request["name"];
                        user.Lastname = Request["lastname"];
                        user.Email = Request["email"];
                        user.DateOfBirth = DateTime.Parse(Request["dateOfBirth"]);
                    }

                       Build.UpdateUser(user);
                }

            Session["user"] = user;

              return RedirectToAction("../FitnessCenter/GetFC");
          
        }

        public ActionResult AddTrainerToFC()
        {
            Dictionary<string, User> users = Build.LoadUser();

            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();


            var u = users[Request["userID"]];


            string fid = Request["fcId"];
            u.CoachFitnessCenterID = fid;

            u.Role = MyWebApp2.Models.Enums.Role.Trener;


            Build.UpdateUser(u);

            ViewBag.fitness = checkFC[fid];

            ViewBag.users = Build.LoadUser();



            return View("UserListFC");
        }

        public ActionResult DeleteUser()
        {
            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();

            Dictionary<string, User> users = Build.LoadUser();

            List<User> usersList = new List<User>();

           

            var u = users[Request["userID"]];

            u.IsDeleted = true;


            Build.UpdateUser(u);

            string fid = Request["fcId"];
            ViewBag.fitness = checkFC[fid];

            ViewBag.users = Build.LoadUser();

            return View("UserListFC");
        }

        public ActionResult UserListFC()
        {
            string fid = Request["fcId"];

            Dictionary<string, User> u = Build.LoadUser();

            List<User> usersList = new List<User>();

            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();


            ViewBag.fitness = checkFC[fid];

            ViewBag.users = u;

            return View("UserListFC");
        }

        public ActionResult UserListGT()
        {
            string name= " ";

            Dictionary<string, User> users = Build.LoadUser();

            List<User> usersList = new List<User>();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();
            
            

            foreach( GroupTraining group in gt.Values) {
                    if(group.ID == Request["gtId"]) {
                            foreach (string userID in group.VisitorsID) {
                                usersList.Add(users[userID]);
                            }

                       // string name = "";
                        name = group.Name;
                    }
                }

            ViewBag.NameGT = name;

            ViewBag.users = usersList;

            return View("UserListGT");
        }

    }
}