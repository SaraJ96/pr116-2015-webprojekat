﻿using MyWebApp2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace MyWebApp2.Controllers
{
    public class FitnessCenterController : Controller
    {
        // GET: FitnessCenter
        public ActionResult GetFC()
        {
            //var fitnessCentersSorted = Build.LoadFC();
            //fitnessCentersSorted.OrderBy(x => x.Value.Name).ToDictionary(x => x.Key, x => x.Value.Name);
            //Session["fitnessCenters"] = fitnessCentersSorted;

              ViewBag.checkFC = Build.LoadFC();

            return View("FCsList");
        }


        public ActionResult GetAllFCs()
        {
            Dictionary<string, User> user = Build.LoadUser();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();

            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();

            List<GroupTraining> gtList = new List<GroupTraining>();

         

            foreach(var g in gt.Values)  {
                if( g.FitnessCenterID == Request["fcId"])  { gtList.Add(g);}
            }

                FitnessCenter fitness = checkFC[Request["fcId"]];
;

            ViewBag.gtList = gtList;

            ViewBag.fitness = checkFC[Request["fcId"]];


            ViewBag.OwnerUserID = user[checkFC[Request["fcId"]].OwnerUserID];



            return View("AllDetails");
        }

        public ActionResult GoToSearchFCsPg()
        {
            return View("FCSearch");
        }

        public ActionResult FCSearch(string name, string address, int? yearOfOpeningMin, int? yearOfOpeningMax)
        {
            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();

            var output = NewSearchFCs(checkFC, name, address, yearOfOpeningMin, yearOfOpeningMax);

            return View("FCsList");

        }

        private Dictionary<string, FitnessCenter> NewSearchFCs(Dictionary<string, FitnessCenter> fitnessCenters, string name, string address, int? yearOfOpeningMin, int? yearOfOpeningMax)
        {
            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();
            Dictionary<string, FitnessCenter> output2 = new Dictionary<string, FitnessCenter>();

            foreach(FitnessCenter fitnessCenter in checkFC.Values)
            {
                if(name != null && name != "")
                {
                    int f = fitnessCenter.Name.ToLower().IndexOf(name.ToLower());
                    if(f < 0) { continue; }
                }
                if(address != null && address != "")
                {
                    int f = fitnessCenter.Address.ToLower().IndexOf(address.ToLower());
                    if(f < 0) { continue; }
                }

                int year = fitnessCenter.YearOfOpening;
                if(yearOfOpeningMin != null && yearOfOpeningMax != null)
                {
                    if(year < yearOfOpeningMin && year > yearOfOpeningMax) { continue; }
                }
                else
                {
                    if(year < yearOfOpeningMin) { continue; }
                    if(year > yearOfOpeningMax) { continue; }
                }

                output2.Add(fitnessCenter.ID, fitnessCenter);
            }
            ViewBag.checkFC = output2;

            
            return output2;
        }

        public ActionResult GoToAddFC()
        {
            return View("AddFC");
        }

        public ActionResult AddFC()
        {
            MyWebApp2.Models.User user = (MyWebApp2.Models.User)Session["user"];
            //try
            //{
                string name = Request["name"];

                string address = Request["address"];

                int yearOfOpening = int.Parse(Request["yearOfOpening"]);

                int monthlyPrice = int.Parse( Request["monthlyPrice"]);

                int yearlyPrice = int.Parse(Request["yearlyPrice"]);

                int trainingPrice = int.Parse(Request["trainingPrice"]);

                int groupPrice = int.Parse(Request["groupPrice"]);

                int personalTrainingPrice=  int.Parse(Request["personalTrainingPrice"]);

                FitnessCenter fc = new FitnessCenter(DateTimeOffset.Now.ToUnixTimeSeconds().ToString(),name, address, yearOfOpening, user.ID, monthlyPrice, yearlyPrice, trainingPrice,   groupPrice   , personalTrainingPrice, false);

                Build.AddFC(fc);

          //  Dictionary<string, User> user = Build.LoadUser();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();

            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();


            user.OwnerFitnessCenterID.Add(fc.ID);

                Build.UpdateUser(user);

                Session["user"] = user;

            List<GroupTraining> gtList = new List<GroupTraining>();

            return RedirectToAction("../FitnessCenter/GetFC");
                

            //}
            //catch (Exception e)
            //{
            //    Session["message"] = e.Message;
            //    return View("../Home/ErrorPg");
            //}
        }

        public ActionResult SaveFC()
        {
            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();
 
           List<GroupTraining> gtList = new List<GroupTraining>();

            string fid = Request["fcId"];
            var fitness = checkFC[fid];

            string name = Request["name"];
            string address = Request["address"];
            int yearOfOpening = int.Parse(Request["yearOfOpening"]);
            int monthlyPrice = int.Parse(Request["monthlyPrice"]);
            int yearlyPrice = int.Parse(Request["yearlyPrice"]);
            int trainingPrice = int.Parse(Request["trainingPrice"]);
            int groupPrice = int.Parse(Request["groupPrice"]);
            int personalTrainingPrice = int.Parse(Request["personalTrainingPrice"]);
            fitness.Name = name;
            fitness.Address = address;
            fitness.YearOfOpening = yearOfOpening;
            fitness.MonthlyPrice =monthlyPrice ;
            fitness.YearlyPrice = yearlyPrice;
            fitness.TrainingPrice = trainingPrice;
            fitness.GroupPrice = groupPrice;
            fitness.PersonalTrainingPrice = personalTrainingPrice;

            Build.UpdateFitnessCenter(fitness);

            return RedirectToAction("../FitnessCenter/GetFC");
        }

        public ActionResult DeleteFC()
        {

            Dictionary<string, User> users = Build.LoadUser();



            string fid = Request["fcId"];

            //try
            //{
               
                foreach(var user in users.Values) {
                    if(user.CoachFitnessCenterID==fid ) {
                        user.IsDeleted = true;

                        Build.UpdateUser(user);
                    }
            }

    //    }
    //        catch(Exception e)
    //        {
    //            Session["message"] = e.Message;
    //            return View("../Home/ErrorPg");
    //}
            //try
            //{ 
                        Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();

                        Dictionary<string, GroupTraining> gt = Build.LoadGT();

                foreach(var groupTraining in gt.Values) {
                    if(groupTraining.FitnessCenterID ==fid &&  DateTime.Compare(groupTraining.TrainingDateTime, DateTime.Now)> 0  )  {
                        ViewBag.fitness = checkFC[fid];

                        return View("UpdateFC");
                    }
                }
            //}
            //catch(Exception e)
            //{
            //    Session["message"] = e.Message;
            //    return View("../Home/ErrorPg");
            //}


            var fitness = checkFC[fid];

            fitness.IsDeleted = true;


            Build.UpdateFitnessCenter(fitness);

            return RedirectToAction("../FitnessCenter/GetFC");
        }

        public ActionResult GoToUpdateFC()
        {
            Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();
            Dictionary<string, User> users = Build.LoadUser();

            string fid = Request["fcId"];

            ViewBag.fitness = checkFC[fid];

            return View("UpdateFC");
        }
        
        public ActionResult CheckIn()
        {
                bool IsCheckIn = true;

                MyWebApp2.Models.User user = (MyWebApp2.Models.User)Session["user"];

                Dictionary<string, FitnessCenter> checkFC = Build.LoadFC();

                Dictionary<string, GroupTraining> gt = Build.LoadGT();

                List<GroupTraining> gtList = new List<GroupTraining>();

                Dictionary<string, Log> log = Build.LoadLog();

                Dictionary<string, User> user2 = Build.LoadUser();

                Dictionary<string, Log> log2 = Build.LoadLog();

                Dictionary<string, FitnessCenter> checkFC2 = Build.LoadFC();

                Dictionary<string, GroupTraining> gt2 = Build.LoadGT();

          
                //try
                //{
                    foreach(var check in log.Values)
                    {
                        if(check.VisitorID==user.ID  &&  check.GroupTrainingID == Request["gtId"]) { IsCheckIn = false; }
                    }

                    if(IsCheckIn==true) {
                        Log logg = new Log(DateTimeOffset.Now.ToUnixTimeSeconds().ToString(), user.ID, Request["gtId"]);

                        Build.AddLog(logg);

                        Build.UserTraining(user, Request["gtId"]);

                        user.VisitorRegisteredGroupTrainingsID.Add(Request["gtId"]);

                        Build.UpdateUser(user);
                    }

                //}catch(Exception e){

                //    Session["message"] = e.Message;
                //    return View("../Home/ErrorPg");
                //}

          
            foreach(var grupni in gt2.Values) {
                if( grupni.FitnessCenterID == Request["fcId"]) { gtList.Add(grupni); }
            }

            ViewBag.OwnerUserID = user2[checkFC[Request["fcId"]].OwnerUserID];

            ViewBag.gtList = gtList;

            ViewBag.fitness = checkFC[Request["fcId"]];

            return View("AllDetails");

            

        }



    }
}