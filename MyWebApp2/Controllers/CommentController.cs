﻿using MyWebApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyWebApp2.Controllers
{
    public class CommentController : Controller
    {
        // GET: Comment
        public ActionResult GoToCommPg()
        {
            return View("CommentsList");
        }

        public ActionResult GoToAddComm()
        {
            Dictionary<string, Comment> comm = new Dictionary<string, Comment>();
            ViewBag.CommFCId = Request["fcId"];

            return View("AddComment");
        }

        public ActionResult AddComment()
        {
          
          //  var fitnesCenter = (FitnessCenter)Session["fitnesCenters"];

            string id = Request["id"];

            var user = (User)Session["user"];

            string fitnessCenterID = Request["fitnessCenterID"];

            string textOfComment = Request["textOfComment"];

            int grade = int.Parse(Request["grade"]);


            
                Build.AddComment(new Comment(DateTimeOffset.Now.ToUnixTimeSeconds().ToString(), user, fitnessCenterID, textOfComment, grade,false));

                ViewBag.checkFC = Build.LoadFC();

                // return View("~/Views/FitnessCenter/FCsList.cshtml");
                return View("../FitnessCenter/FCsList");

           
        }

        public ActionResult Approved()
        {
            //try
            //{
                Dictionary<string, User> users = Build.LoadUser();

                Dictionary<string, Comment> comments = Build.LoadComm();

                Dictionary<string, GroupTraining> gt = Build.LoadGT();

                string comentId = Request["commentID"];

                var cId = comments[comentId];

                cId.IsCommentApproved = true;

                Build.UpdateComment(cId);

                return View("CommentsList");
            //}

            //catch (Exception e)
            //{
            //    Session["message"] = e.Message;
            //    return View("../Home/ErrorPg");
            //}
        }

        public ActionResult Deny()
        {
            //try
            //{
                Dictionary<string, User> users = Build.LoadUser();

                Dictionary<string, Comment> comments = Build.LoadComm();

                Dictionary<string, GroupTraining> gt = Build.LoadGT();

                string comentId = Request["commentID"];

                var cId = comments[comentId];

            cId.IsCommentApproved = false;

                Build.UpdateComment(cId);

                return View("CommentsList");
            //}

            //catch (Exception e)
            //{
            //    Session["message"] = e.Message;
            //    return View("../Home/ErrorPg");
            //}
        }

        private bool IsExpired(GroupTraining gt)
        {
            if (DateTime.Now < DateTime.Parse(gt.TrainingDateTime.ToString()))
            {
                Session["message"] = $"Trening jos traje, ne mozete videti konentare!";
                return false;
            }

            return true;
        }
    }
}