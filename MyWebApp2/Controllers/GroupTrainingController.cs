﻿using MyWebApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyWebApp2.Controllers
{
    public class GroupTrainingController : Controller
    {

        public ActionResult GetAllGTs()
        {
             Dictionary<string, GroupTraining> gt = Build.LoadGT();

            ViewBag.group = gt[Request["gtId"]];


            return View("GTsList");
        }


        public ActionResult GoToAddGT()
        {
                        ViewBag.Dt = DateTime.Now.ToString("yyyy-MM-dd") ;

           // ViewBag.Dt = DateTime.Now.AddDays(3).ToString("yyyy-MM-dd") ;

            return View("AddGT");
        }

        public ActionResult AddGT()
        {
            MyWebApp2.Models.User user = (MyWebApp2.Models.User)Session["user"];

            string name = Request["name"];

           

              Enums.TrainingType type;
            //  string traingType = Request["trainingType"];
            //     TrainingType trainingType = TrainingType.Parse( traingType);

            if (Request["trainingType"] == MyWebApp2.Models.Enums.TrainingType.Yoga.ToString()) {
                type = MyWebApp2.Models.Enums.TrainingType.Yoga;
            } 
 else if(Request["trainingType"] == MyWebApp2.Models.Enums.TrainingType.Les_mills_tone.ToString()) {
                type = MyWebApp2.Models.Enums.TrainingType.Les_mills_tone;
            } else{//if(Request["trainingType"] == MyWebApp2.Models.Enums.TrainingType.Body_pump.ToString()) {
                type = MyWebApp2.Models.Enums.TrainingType.Body_pump;
            }

            int trainingDuration = int.Parse(Request["trainingDuration"]);

            DateTime trainingDateTime = DateTime.Parse(Request["trainingDateTime"]);

            int maxNumberOfVisitors = int.Parse(Request["maxNumberOfVisitors"]);


             GroupTraining gt = new GroupTraining(DateTimeOffset.Now.ToUnixTimeSeconds().ToString(), name, user.ID, type, user.CoachFitnessCenterID, trainingDuration, trainingDateTime, maxNumberOfVisitors, new List<string>() , false);

            Build.AddGT(gt);

            return RedirectToAction("../FitnessCenter/GetFC");
        }


        public ActionResult SaveGT()
        {
            List<GroupTraining> gtLists = new List<GroupTraining>();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();
            
            var groupTraining = gt[Request["gtId"]];

            string name = Request["name"];
            groupTraining.Name = name;

            string traingType = Request["trainingType"];

            if (traingType == MyWebApp2.Models.Enums.TrainingType.Yoga.ToString()) {
                groupTraining.TrainingType = MyWebApp2.Models.Enums.TrainingType.Yoga;
            } else if (traingType == MyWebApp2.Models.Enums.TrainingType.Les_mills_tone.ToString())
            { groupTraining.TrainingType = MyWebApp2.Models.Enums.TrainingType.Les_mills_tone; }
            else {
                groupTraining.TrainingType = MyWebApp2.Models.Enums.TrainingType.Body_pump;
            }

            string fitnessCenterID = Request["fitnessCenterID"];
            groupTraining.FitnessCenterID = fitnessCenterID;

            int trainingDuration = int.Parse(Request["trainingDuration"]);
            groupTraining.TrainingDuration = trainingDuration;

            DateTime trainingDateTime = DateTime.Parse(Request["trainingDateTime"]);
            groupTraining.TrainingDateTime= trainingDateTime;

            int maxNumberOfVisitors =int.Parse(Request["maxNumberOfVisitors"]);
            groupTraining.MaxNumberOfVisitors = maxNumberOfVisitors;  


            Build.UpdateGroupTraining(groupTraining);

            ViewBag.group = groupTraining;

            return View("GTsList");
        }

        public ActionResult DeleteGT()
        {
            List<GroupTraining> gtLists = new List<GroupTraining>();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();

            var groupTraining = gt[Request["gtId"]];

            groupTraining.IsDeleted = true;

            Build.UpdateGroupTraining(groupTraining);

            ViewBag.group = groupTraining;

            return RedirectToAction("../FitnessCenter/GetFC");

        }

        public ActionResult GoToSearchGTsPg()
        {
            return View("GTSearch");
        }

        public ActionResult GTSearch(string name, MyWebApp2.Models.Enums.TrainingType? trainingType, DateTime? trainingDateTime)
        {
            Dictionary<string, GroupTraining> checkGT = Build.LoadGT();

            var output = NewSearchGTs(checkGT, name, trainingType, trainingDateTime);

            return View("GTsList");

        }

    private Dictionary<string, GroupTraining> NewSearchGTs(Dictionary<string, GroupTraining> groupTrainings, string name, MyWebApp2.Models.Enums.TrainingType? trainingType, DateTime? trainingDateTime)
    {
        Dictionary<string, GroupTraining> checkGT = Build.LoadGT();
        Dictionary<string, GroupTraining> output2 = new Dictionary<string, GroupTraining>();
            MyWebApp2.Models.Enums.TrainingType? ttype = MyWebApp2.Models.Enums.TrainingType.Body_pump;


        foreach (GroupTraining groupTraining in checkGT.Values) {
            if(name != null && name != "") {
                int g = groupTraining.Name.ToLower().IndexOf(name.ToLower());
                if (g < 0) { continue; }
            }

            if(trainingType != null && trainingType.ToString() != "")
            {
                    if(trainingType== Enums.TrainingType.Yoga) {
                        ttype = Enums.TrainingType.Yoga;
                    }else if(trainingType == Enums.TrainingType.Les_mills_tone) {
                        ttype = Enums.TrainingType.Les_mills_tone;
                    }
            }

        //    DateTime tdt = groupTraining.TrainingDateTime;
             ////   if (trainingDateTime != null ) { continue; }

                //if (trainingDateTimeMin != null && trainingDateTimeMax != null){
                //    if(DateTime.Compare(groupTraining.TrainingDateTime, trainingDateTimeMin)<=0 && DateTime.Compare(groupTraining.TrainingDateTime, trainingDateTimeMax)>=0) { continue; }
                //} else {
                //    if (DateTime.Compare(groupTraining.TrainingDateTime, trainingDateTimeMin) <= 0) { continue; }
                //    if (DateTime.Compare(groupTraining.TrainingDateTime, trainingDateTimeMax) >= 0) { continue; }
                //}

                output2.Add(groupTraining.ID, groupTraining);
        }
        ViewBag.gtList = output2;

        return output2;
    }

        public ActionResult GTDetails()
        {
            MyWebApp2.Models.User user = (MyWebApp2.Models.User)Session["user"];

            List<GroupTraining> gtLists = new List<GroupTraining>();

            Dictionary<string, GroupTraining> gt = Build.LoadGT();

            //try { 
           if ( user.Role != Enums.Role.Trener) {
                foreach(GroupTraining group in gt.Values)  {
                    if(group.VisitorsID.Contains(user.ID)) {  gtLists.Add(group);
                    }
                }
            }

            if ( user.Role == Enums.Role.Trener) {
                foreach( GroupTraining group in gt.Values){
                    if( group.TrainerID== user.ID ) {  gtLists.Add(group);
                    }
                }
            }
            //} catch (Exception e)
            //{
            //    Session["message"] = e.Message;

            //    return View("../Home/ErrorPg");
            //}
            ViewBag.gtList = gtLists;

            return View("GTDetails");
        }

    }
}